<?php

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.png">

    <title>Cocoma</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="justified-nav.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <div class="masthead">
        <h3 class="text-muted">CoCoMa</h3>
        <ul class="nav nav-justified">
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="history.php">History</a></li>
          <li><a href="setting.php">Settings</a></li>
        </ul>
      </div>

      <!-- Jumbotron -->
      <div class="Jumbotron" style="text-align:center;" >
        <h2>Upload File Here</h2>
    <form action="pictureUpload.php" method="post" class="form-signin" style="text-align:center;" enctype="multipart/form-data">
    <input type="file" name="file" class="btn btn-lg btn-primary" style="margin:0px auto;"/>
    <br>
     <input type="submit" class="btn btn-lg btn-success" value="Upload File"/>
    </form>    
      </div>

      <!-- Example row of columns -->
      <div class="row">
     
      </div>

      <!-- Site footer -->
      <div class="footer">
        <p>&copy; Designed and Developed by <a href="http://www.linkedin.com/profile/view?id=199908706&trk=nav_responsive_tab_profile_pic>">Naijaura inc 2013</a></p>
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>
