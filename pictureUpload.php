<?php
ini_set('display_errors',"1");
require 'upload/ocrInterface.php';
require 'database.php';

//require 'upload/mailsystem.php';
//**********************************************************************************************
 
 
//echo "Please wait while we attempt to upload your file...<br><br>";
 
//**********************************************************************************************
 

$target_path
 = "/var/www/cocoma/upload/";
 
$flag = 0; // Safety net, if this gets to 1 at any point in the process, we don't upload

$filename = $_FILES['file']['name'];
$filesize = $_FILES['file']['size'];
$mimetype = $_FILES['file']['type'];
$ext = end(explode(".", $filename));
 
$filename = htmlentities($filename);
$filesize = htmlentities($filesize);
$mimetype = htmlentities($mimetype);
 
$target_path = $target_path . basename( $filename );
 
if($filename != ""){ 
//echo "Beginning upload process for file named: ".$filename."<br>";
//echo "Filesize: ".$filesize."<br>";
//echo "Type: ".$mimetype."<br><br>"; 
}
 
//First generate a MD5 hash of what the new file name will be
//Force a MP3 extention on the file we are uploading
 
$hashedfilename = md5($filename);
$hashedfilename = $hashedfilename.".jpg";
 
//Check for empty file
if($filename == ""){
//echo  "No File Exists!";
$flag = $flag + 1;
 
}
 
//Now we check that the file doesn't already exist.
$existname = "upload/".$hashedfilename;
 
if(file_exists($existname)){
 
if($flag == 0){
 $error = "Your file already exists on the server!  
Please choose another file to upload or rename the file on your
computer and try uploading it again!";
}
 
$flag = $flag + 1;
}
 
//Whitelisted files - Only allow files with MP3 extention onto server...
/* 
$whitelist = array(".JPG",".png",".gif",".jpeg",".pdf",".jpg");
foreach ($whitelist as $ending) {
 
if(substr($filename, -(strlen($ending))) != $ending) {
$error = "The file type or extention you are trying to upload is not allowed! ";
$flag++;
}
}*/
 
//Now we check the filesize.  If it is too big or too small then we reject it
//MP3 files should be at least 1MB and no more than 6.5 MB
 
if($filesize > 16920600){
//File is too large
 
if($flag == 0){
$error = "The file you are trying to upload is too large!  
Your file can be up to 6.5 MB in size only.  
Please upload a smaller MP3 file or encode your file with a lower bitrate.";

//getHtml($error,"error");
}
 
$flag = $flag + 1;
}

if($filesize < 500){
//File is too small
 
if($flag == 0){
$error = "The file you are trying to upload is too small!
Your file has been marked as suspicious because our system has
determined that it is too small to be a valid MP3 file.
Valid MP3 files must be bigger than 1 MB and smaller than 6.5 MB.";

getHtml($error,"error");
}
 
$flag = $flag + 1;
 
}



//All checks are done, actually move the file...


if($flag == 0){
if(move_uploaded_file($_FILES['file']['tmp_name'],$target_path)){
	
	if(@file_exists(getcwd()."/upload/".$filename)){ 
    //Rename the file and append the date and time to it
	date_default_timezone_set('Africa/Lagos');
	$time = time();
	$actualDate = date('Y-m-d_H_i_s',$time);
	
	//need to append the first part of the file to have date and time 
	$filenamePart = explode('.',$filename);
	$filenamePart[0].=$actualDate;
	$count = count($filenamePart);
	$finalString = '';
	$i=0;
	foreach($filenamePart as $fileName){	
			if(++$i == $count){
				$finalString.=$fileName;
				}	
			else
			$finalString.=$fileName.'.';		
		}		
	$filenamereal = preg_replace('/\s+/', '_', $filename);
	$filenamereal = $finalString;
    rename(getcwd()."/upload/".$filename, getcwd()."/upload/".$finalString);
	 /*exec ("mv /var/chroot/home/content/79/9837479/html/naijaura-jukebox/admin/upload/*.* /var/chroot/home/content/79/9837479/html/naijaura-jukebox/images/");*/
	 
	 //if the extension is a pdf file then send it to the server
	 if($mimetype=="application/pdf"){
		 $response = GetStringFromImage($filenamereal);
		 }
	//check if it is  a gif too because tesseract does not proecess gif
	else if($mimetype=='image/gif'){
			$response = GetStringFromImage($filenamereal);
		}
				 
	//if it is any other type then use tesseract
	else{		
		//upload complete noe run exec code to get string value of image
	 exec('tesseract upload/'.$filenamereal.' upload/'.$actualDate.'');	 
	 //getting the content of the returnig file
	 exec('cat upload/'.$actualDate.'.txt',$ret_val2);
	 
	 $response = $ret_val2;
	 $response = implode(' ',$response);
	 
		}
	 
	 
	 	 	 
	 //echo 'return value:<br>'.$response;
    //echo "The file ".  basename( $filenamereal )."
     //has been uploaded.<br>";
	 //echo 'Now Extracting Text from Image Please Wait....<br>';
	 //gets the string value from the image via ocrInterface	
	// echo $filenamereal; 
	//$response = GetStringFromImage($filenamereal);	 
	
	
	 
	 //echo 'response:'.$response;
	 
	 
	 //gets the closest match via the database
	 
	
	//echo 'getting closest match from database';
	$idOfMatch = getColsestMatch($response); 
	
	
	if($idOfMatch=='error'){
		gutHtml('could not get closest match','error');
		die();
		}
		
	$response = substr($response, 0, 200);	 	 
	 //sends the mail and the attachment using the sendMail function in the mailsystem file
	 $sent= getAndSend($filenamereal, $idOfMatch,$response);
	 
	 if($sent!='sent'){
		 die('the email could not be sent!');		 
		 }
		 
	//echo 'email has been sent <br>';	 
	  gutHtml("email has been sent!","ok");
	 	 	    
    }  
    else{
      //echo "There was an error uploading the file, please try again 1!";
	  gutHtml("There was an error uploading the file, please try again 1!","error");
	  die();
    } 
 
} 

else{
    //echo "There was an error uploading the file, please try again 2!";
	gutHtml("There was an error uploading the file, please try again 2!","error");
	die();
}
 
}
else {
//echo "File Upload Failed!<br>";
/*if($error != ""){
echo $error;
}*/
gutHtml("File Upload Failed","error");
die();
}


//echo the page as usual

function gutHtml($result,$status){

if($status=="error"){
	$text = '<span style="color:red;">'.$result.'</span>';	
	}
else{
	$text = '<span style="color:green;">'.$result.'</span><br>Result Can Be Viewed in History';	
	
	}	
	
echo '<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.png">

    <title>Justified Nav Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="justified-nav.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <div class="masthead">
        <h3 class="text-muted">Result</h3>
        <ul class="nav nav-justified">
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="history.php">History</a></li>
          <li><a href="setting.php">Settings</a></li>
        </ul>
      </div>

      <!-- Jumbotron -->
      <div class="Jumbotron" style="text-align:center;" >
        <h2>Result</h2><br>
		     <h3>'.$text.' </h3>
			 
      </div>

      <!-- Example row of columns -->
      <div class="row">
     
      </div>

      <!-- Site footer -->
      <div class="footer">
        <p>&copy; Designed and Developed by <a href="http://www.linkedin.com/profile/view?id=199908706&trk=nav_responsive_tab_profile_pic>">Naijaura inc 2013</a></p>
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>
';
}
?>